#
# Copyright (C) 2022 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_realme_trinket.mk

COMMON_LUNCH_CHOICES := \
    lineage_realme_trinket-user \
    lineage_realme_trinket-userdebug \
    lineage_realme_trinket-eng
